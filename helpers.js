"use strict";

// My Colors
const grayCyan = "#a1e2e3"

function randRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1) ) + min;
};

function randInt(max) {
  return randRange(0, max - 1);
};

function randomId() {
  return Math.random().toString(36).substr(2, 5);
};

function randomDirection() {
  return ["left", "right", "up", "down"][randInt(4)];
};

function isDirection(instruction) {
  return ["left", "right", "up", "down"].includes(instruction);
};

function oppositeNotPressed(direction, controls) {
  let opposite;
  if (direction == "up") { opposite = "down"; };
  if (direction == "down") { opposite = "up"; };
  if (direction == "left") { opposite = "right"; };
  if (direction == "right") { opposite = "left"; };
  controls.forEach(control => {
    if (control.instruction == opposite && control.pressed) {
      false;
    };
  });
  return true;
};