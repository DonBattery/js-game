"use strict";

var game;
var statsPanel = document.getElementById("StatsPanel");

const Gravity = new Vector(0, 0.7);
const Friction = 0.9;
const MaxSpeed = 10;

function _debug(msg) {
  statsPanel.innerHTML = msg;
}

class Rect {
  constructor(x, y, width, height) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;

    this.collide = this.collide.bind(this);
    this.collideMany = this.collideMany.bind(this);
  };

  collide(otherRect) {
    return (this.x < otherRect.x + otherRect.width) && (this.x + this.width > otherRect.x) && (this.y < otherRect.y + otherRect.height) && (this.y + this.height > otherRect.y);
  };

  collideMany(otherRects) {
    for (let index = 0; index < otherRects.length; index++) {
      if (this.collide(otherRects[index])) {
        return true;
      };
    };
    return false;
  };
};

class GameElement extends Rect {
  constructor(parentId, id, type, x, y, width, height, color) {
    super(x, y, width, height);
    this.parentId = parentId;
    this.id = id;
    this.type = type;
    this.color = color;

    this.draw = this.draw.bind(this);
  };

  draw(ctx) {
    ctx.fillStyle = this.color;
    ctx.fillRect(this.x, this.y, this.width, this.height);
  };
};

class Wall extends GameElement {
  constructor(x, y, width, height, color) {
    super("world", randomId(), "wall", x, y, width, height, color);
  };
};

class Player extends GameElement {
  constructor(name, color, x, y, controls) {
    super("world", name, "player", x, y, 150, 40, color);
    this.controls = controls;
    this.score = 0;

    this.draw = this.draw.bind(this);
    this.update = this.update.bind(this);
  };

  draw(ctx) {
    ctx.font = "18px Arial";
    ctx.fillStyle = "black";
    ctx.fillText(`${this.id} score:${this.score}`, this.x, this.y + 18);
  };

  update() {
    let tanks = game.getChildElementsByType(this.id, "tank");
    if (tanks.length != 1) { return };
    let tank = tanks[0];
    this.controls.forEach(control => {
      if (control.pressed) {
        if (isDirection(control.instruction) && oppositeNotPressed(control.instruction, this.controls)) {
          if (control.instruction == "left") {
            tank.vector.x = -tank.speed;
          };
          if (control.instruction == "right") {
            tank.vector.x = tank.speed;
          };
          if (control.instruction == "up") {
            if (!tank.inAir) {
              tank.vector.y = -10;
            };
          };
          _debug(`Instruction: ${control.instruction} Tank vec X: ${tank.vector.x} Tank vec Y:  ${tank.vector.y}`);
        };
      };
    });
    // console.log(`Player controls ${JSON.stringify(this.controls)} Tank: ${JSON.stringify(tank)}`);
  };
};

class MovingGameElement extends GameElement {
  constructor(parentId, id, type, x, y, width, height, color, speed) {
    super(parentId, id, type, x, y, width, height, color);
    this.speed = speed;
    this.inAir = true;
    this.vector = new Vector(0, 0);

    this.move = this.move.bind(this);
  };

  move(colliders) {
    // let collided = false;

    if (this.inAir) {
      this.vector.addTo(Gravity);
    }

    if ((Math.abs(this.vector.x) > MaxSpeed) || (Math.abs(this.vector.y) > MaxSpeed)) {
      this.vector.setMagnitude(MaxSpeed);
    }

    let nextPosition = new Rect(Math.round(this.x + this.vector.x), Math.round(this.y + this.vector.y), this.width, this.height);

    let backwards = this.vector.copy().invert().normalize();
    while ((nextPosition.collideMany(colliders)) && !((this.x == nextPosition.x) && (this.y == nextPosition.y))) {
      nextPosition.x += backwards.x;
      nextPosition.y += backwards.y;
    };

    this.x = nextPosition.x;
    this.y = nextPosition.y;

    nextPosition.y += 1;
    if (nextPosition.collideMany(colliders)) {
      this.vector.y = 0;
      this.inAir = false;
    } else {
      this.inAir = true;
    };

    this.vector.setMagnitude(this.vector.getMagnitude() * Friction);
  };

};

class Tank extends MovingGameElement {
  constructor(parentId, id, x, y, color) {
    super(parentId, id, "tank", x, y, 16, 16, color, 4);

    this.update = this.update.bind(this);
  }

  update() {
    let colliders = [];
    let walls = game.getElementsByType("wall");
    let otherTanks = game.getOthersFromType(this.id, "tank");

    colliders = colliders.concat(walls, otherTanks);

    this.move(colliders);
  };

};

// Készit egy borderWidth szélességű keretet a játéknak falakból
function gameBorder(borderWidth, color) {
  let topWall = new Wall(0, 0, game.width, borderWidth, color);
  let botWall = new Wall(0, game.height - borderWidth, game.width, borderWidth, color);
  let leftWall = new Wall(0, borderWidth, borderWidth, game.height - borderWidth, color);
  let rightWall = new Wall(game.width - borderWidth, borderWidth, borderWidth, game.height - borderWidth, color);
  game.addElements([topWall, botWall, leftWall, rightWall]);
};

function setupGame() {

  game = new Game(320, 200);

  let player1 = new Player("Miki", "red", 5, 15, [
    {
      key: "ArrowUp",
      instruction: "up",
      pressed: false,
    },
    {
      key: "ArrowDown",
      instruction: "down",
      pressed: false,
    },
    {
      key: "ArrowLeft",
      instruction: "left",
      pressed: false,
    },
    {
      key: "ArrowRight",
      instruction: "right",
      pressed: false,
    },
  ]);

  let player2 = new Player("Sanyi", "black", 180, 33, [
    {
      key: "w",
      instruction: "up",
      pressed: false,
    },
    {
      key: "s",
      instruction: "down",
      pressed: false,
    },
    {
      key: "a",
      instruction: "left",
      pressed: false,
    },
    {
      key: "d",
      instruction: "right",
      pressed: false,
    },
  ]);

  let tank1 = new Tank("Miki", randomId(), 200, 72, "red");

  let tank2 = new Tank("Sanyi", randomId(), 280, 23, "blue");

  game.addElements([player1, player2, tank1, tank2]);

  gameBorder(2, "yellow");
  // for (let index = 0; index < 17; index++) {
  //   let wall = new Wall(randRange(30, 700), randRange(30, 600), randRange(10, 120), randRange(10, 120), "yellow");
  //   game.addElement(wall);
  // };


  document.addEventListener("keydown", game.onKey, false);
  document.addEventListener("keyup", game.onKey, false);

  window.addEventListener('resize', game.resize, false);
  window.addEventListener('orientationchange', game.resize, false);

  game.resize();

  // ~33 FPS
  game.frames = setInterval(game.update, 33);
};

window.addEventListener("load", setupGame);
