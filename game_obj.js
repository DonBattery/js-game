"use strict";

class Game {
  constructor(width, height) {
    this.gameArea = document.getElementById("GameArea");
    this.statsPanel = document.getElementById("StatsPanel");
    this.canvas = document.getElementById("GameCanvas");
    this.ctx = this.canvas.getContext("2d");

    this.width = width;
    this.height = height;

    this.widthToHeight = width / height;

    this.gameElements = [];

    this.addElement = this.addElement.bind(this);
    this.addElements = this.addElements.bind(this);
    this.getElementById = this.getElementById.bind(this);
    this.getElementsByType = this.getElementsByType.bind(this);
    this.getChildElements = this.getChildElements.bind(this);
    this.getChildElementsByType = this.getChildElementsByType.bind(this);
    this.getOthersFromType = this.getOthersFromType.bind(this);
    this.removeElementById = this.removeElementById.bind(this);
    this.removeElementsByType = this.removeElementsByType.bind(this);
    this.removeChildren = this.removeElementsByType.bind(this);

    this.onKey = this.onKey.bind(this);

    this.clear = this.clear.bind(this);
    this.resize = this.resize.bind(this);
    this.update = this.update.bind(this);
  }

  clear() {
    this.ctx.fillStyle = "white";
    this.ctx.fillRect(0, 0,  this.width, this.height);
  }

  // Ez a ravasz metódus a böngésző átméretezésekor, vagy mikor megváltozik az orientációja (a telefont / tabletet
  // elforgatják 90°-al) újra méretezi nekünk a canvast, mégpedig úgy, hogy a lehető legnagyobb területet foglaja el
  // az ablakból, de tartsa meg az eredeti arányait. Végül a betűméretet a canvas-hoz igazítom és
  // a this.ctx.scale(scaleX, scaleY)-al beállítom, hogy mekkora legyen egy "pixel" a valóságban
  // https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/scale
  // https://www.html5rocks.com/en/tutorials/casestudies/gopherwoord-studios-resizing-html5-games/#disqus_thread
  resize() {
    let newWidth = window.innerWidth;
    let newHeight = window.innerHeight;
    let newWidthToHeight = newWidth / newHeight;

    if (newWidthToHeight > this.widthToHeight) {
        newWidth = newHeight * this.widthToHeight;
        this.gameArea.style.height = newHeight + "px";
        this.gameArea.style.width = newWidth + "px";
    } else {
        newHeight = newWidth / this.widthToHeight;
        this.gameArea.style.width = newWidth + "px";
        this.gameArea.style.height = newHeight + "px";
    }

    this.gameArea.style.marginTop = (-newHeight / 2) + "px";
    this.gameArea.style.marginLeft = (-newWidth / 2) + "px";

    this.canvas.width = newWidth;
    this.canvas.height = newHeight;
    this.gameArea.style.fontSize = (newWidth / this.width / 2) + 'em';
    this.ctx.scale(this.canvas.width / this.width, this.canvas.height / this.height);
  }

  // A játék szive az update metódus, a setupGame meghívása végén bekerül egy setInterval-ba és 25 milliszekundumonként
  // meg lesz hívva. Mivel 1000 milliszekundum van egy másodpercben ez 40 FPS-t jelent (frame per secundum / képkocka per másodperc)
  update() {
    // minden képkocka kirajzolása előtt állítsuk az ablak közepébe a játékteret és töröljük le.
    this.clear();
    // itt végigmegyek az összes játék elemen és megnézem, hogy van-e neki saját update metódusa, ha igen meghívom.
    this.gameElements.forEach(element => {
      if (typeof element.update === "function") {
        element.update();
      };
      // kirajzolom a játékelemet (itt meg kéne oldani a Z indexet, úgy hogy a játékelemeket először a szerint rendezem és csak utána megyek rajtuk végig)
      element.draw(this.ctx);
    });
  };

  // A játék ezen metódusai mind a gameElements listát kezelik
  // Betesznek, kikeresnek, kidobnak bizonyos elemeket a listából.
  addElement(element) {
    this.gameElements.push(element);
  };

  addElements(elements) {
    this.gameElements = this.gameElements.concat(elements);
  };

  getElementById(elementId) {
    return this.gameElements.find(element => element.id == elementId);
  };

  getElementsByType(elementType) {
    return this.gameElements.filter(element => element.type == elementType);
  };

  getChildElements(elementId) {
    return this.gameElements.filter(element => element.parentId == elementId);
  };

  getChildElementsByType(elementId, type) {
    return this.gameElements.filter(element => (element.parentId == elementId && element.type == type));
  };

  getOthersFromType(elementId, type) {
    return this.gameElements.filter(element => (element.id != elementId && element.type == type));
  };

  removeElementById(elementId) {
    this.gameElements = this.gameElements.filter(element => element.id != elementId);
  };

  removeElementsByType(elementType) {
    this.gameElements = this.gameElements.filter(element => element.type != elementType);
  };

  removeChildren(elementId) {
    this.gameElements = this.gameElements.filter(element => element.parentId != elementId);
  };

  // Minden gomb lenyomásakor vagy elengedésekor meg fogjuk hívni ezt a metódust,
  // ami végigmegyek az összes játékos összes controlján és ha a gomb (event.key)
  // egyezik a játékos gombjával (control.key) akkor a control changed preperty-jét
  // igazra állítom, és a pressed property-jét igazra állítom ha a gombot lenyomták
  // és hamisra ha felengedték.
  onKey(event) {
    this.getElementsByType("player").forEach(player => {
      player.controls.forEach(control => {
        if (event.key == control.key) {
          control.pressed = event.type == "keydown";
        };
      });
    });
  };

};
